
import os
import cv2
import numpy as np
import pathlib
import shutil
from PIL import Image

from img import get_contours, get_rects




# https://techvidvan.com/tutorials/detect-objects-of-similar-color-using-opencv-in-python/

# Kanttarellin värit
lower_bound = np.array([10, 100, 200])	 
upper_bound = np.array([25, 255, 255])

fontScale = 1
f_color_bad = (0,0,200)
f_color_good = (0, 255, 0)
thickness = 1
font = cv2.FONT_HERSHEY_COMPLEX_SMALL

c_color = (255, 255, 255)

margin_w = 2
margin_h = 2 

def process_img(path, predict=False, model=None):

    img = cv2.imread(path)
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    _, contours = get_contours(img, True)
    rects = get_rects(img, contours)

    output = cv2.drawContours(img, contours, -1, (0, 0, 255), 1)

    file_name = os.path.basename(path)
    p = pathlib.Path(path)
    root_path = p.parts[0]

    idx = 0
    for rect in rects:
        x,y,w,h = rect
        if w > 3 * h or h > 3 * w:
            continue
        if w < 10 or h < 10:
            continue

        img_h, img_w, img_channels = img.shape

        cv2.rectangle(output, (x-margin_w,y-margin_h), (x+w+margin_w,y+h+margin_w), c_color, 1, lineType=cv2.LINE_AA)
        pala  = img.copy()[
                    max(0, y-15) : min(img_h, y+w+15), 
                    max(0, x-15) : min(img_w, x+w+15)
                ].copy()
        
        #img = cv2.Canny(img, 20, 100)
        #pala = cv2.cvtColor(pala, cv2.COLOR_BGR2GRAY)
        #pala = cv2.adaptiveThreshold(pala, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

        crop_path = root_path + "/ulos/osat/" + file_name + "-" + str(idx) + ".jpeg"
        cv2.imwrite(crop_path, pala)
        idx = idx + 1
        file_name = os.path.basename(path)
        p = pathlib.Path(path)
        root_path = p.parts[0]

        if predict:
            _img = Image.fromarray(pala)
            _img = _img.convert('L')
            _img = _img.resize((128, 128), Image.ANTIALIAS)
            _img_arr = np.array(_img).reshape(-1, 128, 128, 1)

            prediction = model.predict(_img_arr, verbose=False)

            f_color = f_color_good if prediction.item(0) >= 0.7 else f_color_bad
            likehood = str(round(prediction.item(0), 2))
            print(likehood)

            cv2.putText(img,  likehood + "%", (x-margin_w, y-margin_h), font, fontScale, f_color, thickness, cv2.LINE_AA)

    try:
        cv2.imwrite(root_path + "/ulos/" + os.path.basename(path), img)
        print(path)
    except Exception as e:
        print(e)
        print(path)
    return

from kysy import ask_yes_no
if ask_yes_no("Käsittele harjoituskuvat", default_value="n"):
    files = os.listdir("harjoitus")
    for f in files:
        if f.endswith("jpeg") or f.endswith("jpg") or f.endswith("png"):
            process_img("harjoitus/" + f, predict=False)
    print("Harjoitukset käsitelty, saa sulkea.")



files = os.listdir("tunnistus")
shutil.rmtree("tunnistus/ulos/osat/")
os.mkdir("tunnistus/ulos/osat")
predict = True 

from keras.models import Sequential, load_model           
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
#from keras.layers.normalization import BatchNormalization
from tensorflow.keras.layers import BatchNormalization

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)


try:
    if predict is True:
        model = load_model("tunnistin/model.h5")
except Exception as err:
    print("model.h5 cannot be loaded, won't predict.")
    print(err)
    predict=False
    #cv2.imshow("Image", img)
    #cv2.waitKey(0)

for f in files:
    if f.endswith("jpeg") or f.endswith("jpg") or f.endswith("png"):
        process_img("tunnistus/" + f, predict=True, model=model)

