from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
#from keras.layers.normalization import BatchNormalization
from tensorflow.keras.layers import BatchNormalization

from PIL import Image
from random import shuffle, choice
import numpy as np
import os

import string
import random

IMAGE_SIZE = 128 
IMAGE_DIRECTORY = '../tunnistus/ulos/osat/'

def load_img(image_path):
    print("Loading " + image_path)
    img = Image.open(image_path)
    img = img.convert('L')
    img = img.resize((IMAGE_SIZE, IMAGE_SIZE), Image.ANTIALIAS)

    return np.array(img).reshape(-1, IMAGE_SIZE, IMAGE_SIZE, 1)



print('Loading model...')
model = load_model("model.h5")


print("Predicting..")


for r, d, f in os.walk(IMAGE_DIRECTORY):
    for file in f:

        img_path = os.path.join(r, file)

        img = load_img(os.path.join(r, file))

        res = model.predict(img, verbose=False)

        likely_kanttis= str(round(res.item(0), 3))
        #likely_not = str(round(res.item(1), 3))
        print(likely_kanttis)

        print(likely_kanttis)
        #print(likely_not)
        print("")

        if file.startswith(likely_kanttis + " -"):
            continue

        new_name = likely_kanttis + " - " + ''.join(random.choice(string.ascii_letters) for x in list(range(5)))

        os.rename(img_path, os.path.join(r, new_name))
