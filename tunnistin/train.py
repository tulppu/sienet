from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Flatten, GaussianNoise
from keras.layers import Conv2D, MaxPooling2D
#from keras.layers.normalization import BatchNormalization
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
from PIL import Image
from random import shuffle, choice, seed
import numpy as np
import os
#from keras.utils import plot_model


#import tensorflow as tf
#tf.sysconfig.get_build_info()

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

import random


IMAGE_SIZE = 128 
IMAGE_DIRECTORY = "../harjoitus/ulos/osat/"

def get_handled_files_amount(files):
  return len(files)

def label_img(name):
  if name == 'hyvät': return np.array([1, 0])
  else: return np.array([0, 1])

def load_data():
  print("Loading images...")
  train_data = []
  directories = next(os.walk(IMAGE_DIRECTORY))[1]

  for dirname in directories:

    if dirname == "sivutetut":
          continue

    if dirname.endswith(".off"):
      print("Skipping " + dirname)
      continue

    print("Loading {0}".format(dirname))
    #file_names = next(os.walk(os.path.join(IMAGE_DIRECTORY, dirname)))[2]
    
    files = os.listdir(os.path.join(IMAGE_DIRECTORY, dirname))
    #files = random.choices(files, k=2048)
    
    for image_name in files:
        #for i in range(get_handled_files_amount(file_names)):
      #image_name = choice(file_names)
      image_path = os.path.join(IMAGE_DIRECTORY, dirname, image_name)
      label = label_img(dirname)
      if "DS_Store" not in image_path:
        img = Image.open(image_path)
        img = img.convert('L')
        img = img.resize((IMAGE_SIZE, IMAGE_SIZE), Image.ANTIALIAS)
        train_data.append([np.array(img), label])

  return train_data


def create_model():
  model = Sequential()
  model.add(Conv2D(32, kernel_size = (3, 3), activation='relu',
                   input_shape=(IMAGE_SIZE, IMAGE_SIZE, 1)))
  #model.add(GaussianNoise(0.05))

  model.add(MaxPooling2D(pool_size=(2,2)))
  model.add(BatchNormalization())
  model.add(Conv2D(64, kernel_size=(3,3), activation='relu'))
  model.add(MaxPooling2D(pool_size=(2,2)))
  model.add(BatchNormalization())
  model.add(Conv2D(128, kernel_size=(3,3), activation='relu'))
  model.add(MaxPooling2D(pool_size=(2,2)))
  model.add(BatchNormalization())
  model.add(Conv2D(128, kernel_size=(3,3), activation='relu'))
  model.add(MaxPooling2D(pool_size=(2,2)))
  model.add(BatchNormalization())
  model.add(Conv2D(64, kernel_size=(3,3), activation='relu'))
  model.add(MaxPooling2D(pool_size=(2,2)))
  model.add(BatchNormalization())
  model.add(Dropout(0.2))
  model.add(Flatten())
  model.add(Dense(128, activation='relu'))
  model.add(Dropout(0.5))
  #model.add(Dense(64, activation='relu'))
  #model.add(Dense(2, activation = 'softmax'))

  model.add(Dense(512, activation= 'relu')),
  model.add(Dense(2, activation= 'softmax'))

  return model

import sys
sys.path.append("../")
from kysy import ask, ask_yes_no, int_validator

retrain_model   = ask_yes_no("Kouluta vanhaa mallia?", default_value="k") 
epoch_count     = ask("Eepoksia?",  validator_cb=int_validator, default_value=4)
batch_size      = ask("Eräkoko?",   validator_cb=int_validator, default_value=32) 


print("Ladataan kuvia..")
training_data = load_data()
training_images = np.array([i[0] for i in training_data])#.reshape(-1, IMAGE_SIZE, IMAGE_SIZE, 1)
training_labels = np.array([i[1] for i in training_data])

print("..." +str(len(training_images)) + " kuvaa ladattu.")
print()

# Define a ReduceLROnPlateau callback
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=3, min_lr=0.0001)


if retrain_model:
    model = load_model("model.h5")
    print("Vanha malli ladattu")
    print("..tasoja " + str(len(model.layers)))

    for l in model.layers:
        l.trainable = True
else:
    print("Luodaan uusi malli")
    model = create_model()
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit(training_images,
          training_labels, 
          batch_size=batch_size, 
          epochs=epoch_count, 
          verbose=True, 
          shuffle=True, 
          use_multiprocessing=True,
          validation_split=0.1,
          callbacks=[ reduce_lr ],
)
model.save("model.h5")
#plot_model(model, show_shapes=True, to_file='model.png')
