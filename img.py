
import os
import cv2
import numpy as np
import pathlib
import shutil
from PIL import Image

_lower_bound = np.array([10, 100, 200])	 
_upper_bound = np.array([25, 255, 255])

def get_contours(img, combine=True, lower_bound=_lower_bound, upper_bound=_upper_bound):

    contours =  contours_expanded = None

    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    dup = img.copy()
    mask = cv2.inRange(img_hsv, lower_bound, upper_bound)

    kernel = np.ones((7,7),np.uint8)

    # Remove unnecessary noise from mask
    #cv2.erode(mask, kernel)
    #mask = cv2.morphologyEx(mask, cv2.MORPH_DILATE, kernel)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    #mask = cv2.morphologyEx(mask, cv2.MORPH_DILATE, kernel)
    
    contours, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)


    if combine:
        for c in contours:
            poly = cv2.approxPolyDP(c, 3, True)
            rect = cv2.boundingRect(poly)
            #print(rect)
            #c_mask = np.array([img.shape[0],img.shape[1], 255], dtype = "uint8")
            x,y,w,h = rect
            cv2.rectangle(mask, (x-20,y-20), (x+w+20, y+h+20), 255, -1)  
            #print("öö?")

    segmented_img = cv2.bitwise_and(img, img, mask=mask)

    contours_expanded, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return contours, contours_expanded

def get_rects(img, contours): 
    #img = cv2.imread(path)
    #contours, contours_expanded = get_contours(img)
    #output = cv2.drawContours(img, contours, -1, (0, 0, 255), 1)

    color = (255, 255, 255)
    margin = 25

    #"file_name = os.path.basename(path)
    #p = pathlib.Path(path)
    #root_path = p.parts[0]

    rects = []

    for idx, c in  enumerate(contours):
        poly = cv2.approxPolyDP(c, 3, True)
        rect = cv2.boundingRect(poly)
        #print(rect)
        rects.append(rect) 
        continue

    return rects 

    #cv2.imshow("Output", output)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    #img = cv2.Canny(img, 20, 100)
    #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    try:
        cv2.imwrite(root_path + "/ulos/" + os.path.basename(path), img)
        print(path)
    except Exception as e:
        print(e)
        print(path)
