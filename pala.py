from PIL import Image

lower_bound = np.array([10, 100, 200])	 
upper_bound = np.array([25, 255, 255])

def paloita(path):
    img = cv2.imread(path)
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    dup = img.copy()
    """
    h,s,v = cv2.split(img_hsv)
    h_new = h+10
    img_NZ_merged = cv2.merge((h_new, s,v))
    img_hsv = cv2.cvtColor(img_NZ_merged, cv2.COLOR_BGR2HSV)
    """
    mask = cv2.inRange(img_hsv, lower_bound, upper_bound)

    kernel = np.ones((7,7),np.uint8)

    # Remove unnecessary noise from mask
    #cv2.erode(mask, kernel)
    #mask = cv2.morphologyEx(mask, cv2.MORPH_DILATE, kernel)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    #mask = cv2.morphologyEx(mask, cv2.MORPH_DILATE, kernel)

    # Segment only the detected region
    #segmented_img = cv2.bitwise_and(img, img, mask=mask)
    #cannied = cv2.Canny(img.copy(), 20, 100)
    #cannied = cv2.cvtColor(cannied, cv2.COLOR_BGR2GRAY)
    #cannied = cv2.adaptiveThreshold(cannied, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    
    contours, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for c in contours:
        poly = cv2.approxPolyDP(c, 3, True)
        rect = cv2.boundingRect(poly)
        #print(rect)
        #c_mask = np.array([img.shape[0],img.shape[1], 255], dtype = "uint8")
        x,y,w,h = rect
        cv2.rectangle(mask, (x-20,y-20), (x+w+20, y+h+20), 255, -1)  
        #print("öö?")

    #cv2.imshow("aa?", mask)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    segmented_img = cv2.bitwise_and(img, img, mask=mask)

    contours, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    output = cv2.drawContours(img, contours, -1, (0, 0, 255), 1)

    color = (255, 255, 255)
    margin = 25

    file_name = os.path.basename(path)
    p = pathlib.Path(path)
    root_path = p.parts[0]

    palat = []

    for idx, c in  enumerate(contours):
        poly = cv2.approxPolyDP(c, 3, True)
        rect = cv2.boundingRect(poly)
        #print(rect)

        x,y,w,h = rect
        if w > 3 * h or h > 3 * w:
            continue
        if w < 10 or h < 10:
            continue

        margin_w = 2
        margin_h = 2 

        img_h, img_w, img_channels = img.shape

        cv2.rectangle(output, (x-margin_w,y-margin_h), (x+w+margin_w,y+h+margin_w), color, 1, lineType=cv2.LINE_AA)
        pala  = dup[
                    max(0, y-15) : min(img_h, y+w+15), 
                    max(0, x-15) : min(img_w, x+w+15)
                ].copy()
        palat += pala

    return palat
