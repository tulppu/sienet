import numpy as np

import os
import shutil

#%matplotlib notebook

KUVAT = os.listdir("harjoitus/ulos/osat")

JUURI = "harjoitus/ulos/osat/"

POLKU_HYVAT         = JUURI + "hyvät/"
POLKU_HUONOT        = JUURI + "huonot/"
POLKU_SIVUTETUT     = JUURI + "sivutetut/" 

for kuva in KUVAT:
    polku = JUURI + kuva
    
    print(polku)

    if not os.path.isfile(polku):
        continue

    jo_lajiteltu = os.path.exists(POLKU_HYVAT + kuva) \
        or os.path.exists(POLKU_HUONOT + kuva) \
        or os.path.exists(POLKU_SIVUTETUT + kuva)

    if jo_lajiteltu:
        print(polku + " jo lajiteltu.")
        continue

    import matplotlib.pyplot as plt
    from matplotlib.widgets import Button
    import matplotlib.image as mpimg

    def hyvaksi(*args):
        shutil.copy(polku, POLKU_HYVAT + kuva) 
        plt.close()

    def huonoksi(*args):
        shutil.copy(polku, POLKU_HUONOT + kuva) 
        plt.close()

    def sivuta(*args):
        shutil.copy(polku, POLKU_SIVUTETUT + kuva) 
        plt.close()

    if "huono_" in polku:
        huonoksi()
        continue
    
    img = mpimg.imread(polku)
    print(len(img.shape))    
    
    if len(img.shape) < 3:
        sivuta()
        continue


    height, width, depth = img.shape
    dpi = plt.rcParams['figure.dpi']
    figsize = width / float(dpi), height / float(dpi)
    #plt.figure(figsize=figsize)
    print(figsize)
    figure, ax = plt.subplots(figsize=figsize)


    plt.imshow(img, aspect="auto")

    # https://matplotlib.org/stable/gallery/widgets/buttons.html
    axhyva = figure.add_axes([0.6, 0.05, 0.1, 0.075])
    axsivuta = figure.add_axes([0.7, 0.05, 0.1, 0.075])
    axhuono = figure.add_axes([0.81, 0.05, 0.1, 0.075])

    bnhyva = Button(axhyva, 'Hyvä')
    bnhyva.on_clicked(hyvaksi)

    bnsivuta = Button(axsivuta, 'Sivuta')
    bnsivuta.on_clicked(sivuta)

    bnhuono = Button(axhuono, 'Huono')
    bnhuono.on_clicked(huonoksi)

    plt.show()
    print("??")
