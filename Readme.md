Käyttö:

python3 kanttikset.py - prosessoi kuvat kansioista harjoitus ja tunnistus, kroppaa kuvista keltaisen ja kopio kroppaukset ulos/osat -kansioon. Luo ulos-kansioihin kopiot kuvista näyttäen kuvat kokonaisina kropattuine alueineen, sekä lisää ennusteet tunnistus/ulos-kansion kuviin viimeeiseksi koulutetulla mallilla.


python3 lajittelu.py - käy yksitellen läpi harjoitus/ulos/palat -kansion kuvat pyytäen merkitsemään kuvan joko hyväksi tai huonoksi. Hyväksi tulee merkitä ainoastaan kuva, joka mielellään ainakin lähes kokonainen ja selkeästi kanttarelliksi tunnistettava. Kuvista tehdään valinnan mukaan kopiot harjoitukset/ulos/palat/hyvät|huonot -kansioon. Näiden kansioden sisältöä saa poistaa tai siirtää käsin. Jos kuva on kertaalleen arvioitu, ei sen uudelleenarviointia pyydetä. Tiedostonimeen voidaan lisätä etuliite "huono_", jolloin sitä ei tarvitse läpikäydä käsin ja kaikki siitä luodut kroppaukset lajittuvat automaattisesti huonot -kansioon.

Kun harjoitukset/ulos/ -kansioon ollaan generoitu uusia kuvia, tulee kanttikset.py ja lajittelu.py suorittaa, jotta uutta materiaalia voidaan käyttää hyväksi mallin harjoittamisessa.

tunnistin -kansiosta löytyy koneoppihommelit.

python3 train.py kouluttaa mallin harjoitus/ulos/osat/hyvät|huonot -kansioiden pohjalta.
python3 predict.py käy tunnistus/ulos/osat -kansion sisällön antaen oman arvioinsa asteikolla 1.0-0.0 kuvan kanttarellimäisyydestä.

tunnistus ja harjoitus -kansiossa ei saa olla samoja kuvia.  


