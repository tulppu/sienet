
def ask(question, validator_cb=None, valid_inputs = None, default_value=None): 

    if default_value:
        question += "(" + str(default_value) + ")"

    input_res = input(question) or default_value

    if valid_inputs:
        if input_res in valid_inputs:
            return input_res
        else:
            return ask(question, validator_cb, valid_inputs, default_value)

    if validator_cb:
        is_valid, input_processed = validator_cb(input_res) 
        if not is_valid:
            return ask(question, validator_cb)

        return input_processed 

def ask_yes_no(question, default_value="k"):
    return ask(question=question,
        default_value=default_value,
        valid_inputs=["y", "k", "n", "e"]
    ) in ["k", "y"]

def int_validator(input_val):
    if int(input_val) == input_val:
        return True, input_val

    if not input_val.isdigit():
        return False, None 
    return True, int(input_val)

